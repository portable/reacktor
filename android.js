const vis = new Visualiser(1200, 1200);

vis.drawGrid();

const convertDirectionsToPath = (startCoords, directions) => {
  const path = [];

  let { x, y } = startCoords;

  path.push({ x, y, d: 'B' });

  directions.map(direction => {
    switch (direction) {
      case 'U':
        path.push({x, y: --y, d: direction });
        break;

      case 'D':
      path.push({ x,y: ++y, d: direction });
        break;

      case 'L':
      path.push({ x: --x, y, d: direction });
        break;
        
      case 'R':
      path.push({x: ++x, y, d: direction });
        break;

      default:
        path.push({ x, y, d: direction });
        break;
    }
  });

  return path;
}

const fragments = [];

lines.forEach(line => {
  const split = line.split(' ');

  if (split.length === 2) {
    const [startX, startY] = split[0].split(',');
    const startCoords = { x: Number(startX), y: Number(startY) };
    const directions = split[1].split(',');
    const path = convertDirectionsToPath(startCoords, directions);

    vis.drawPath(path);

    fragments.push({ start: startCoords, path });
  }
});

const allValidNodes = fragments.reduce((acc, fragment) => {
  return [...acc, ...fragment.path];
}, []);


const findStartCoords = (nodes) => {
  return nodes.find(node => {
    if (node.d === 'S') {
      console.log(`Start co-ords found: ${node.x},${node.y}`);
      return true;
    }

    return false;
  });
};

const findFinishCoords = (nodes) => {
    return nodes.filter(node => {
    if (node.d === 'F') {
      console.log(`Finish co-ords found: ${node.x},${node.y}`);
      return true;
    }

    return false;
  });
};

const startCoords = findStartCoords(allValidNodes);
console.log('Start Coords', startCoords);

const finishCoords = findFinishCoords(allValidNodes);
console.log('Finish Coords', finishCoords);

// check if we've reached the finish zone
const areInFinishZone = ({x, y}) => !!finishCoords.find(node => node.x === x && node.y === y);

let interval;
let c = 0;
let currentCoords = startCoords;
let visitedNodes = [];

console.log('All valid nodes', allValidNodes);

vis.gotoStart(startCoords.x, startCoords.y);






// find the first valid co-ordinate that hasn't already been used
const findNextValidCoords = ({ x, y }) => {
  // @TODO: IMPLEMENT ME! (hint 1 on slack)
};


const walk = () => {
  // check if we have reached the end
  if (areInFinishZone(currentCoords)) {
    console.log('SUCCESS!', currentCoords);

    clearInterval(interval);
    return true;
  }

  const next = findNextValidCoords(currentCoords);


  // @TODO: IMPLEMENT ME! (hint 2 on slack)


  // we have a valid next coordinate so move there
  if (next) {
    vis.lineTo(currentCoords.x, currentCoords.y, next.x, next.y);

    currentCoords = next;
  } else { // dead end! we need to find where to continue searching from
    vis.endPath();
    vis.drawDeadEnd(currentCoords.x, currentCoords.y);


    // @TODO: IMPLEMENT ME! (hint 3 on slack)


    vis.drawDeadEndToNextValidNode(currentCoords.x, currentCoords.y, lastBranchNode.x, lastBranchNode.y);

    currentCoords = lastBranchNode;
  }


  // circuit breaker - makes sure we don't get stuck in an infinite loop :)
  if (c > 5000) {
    console.log('c > 5000, breaking');

    vis.endPath();
    
    clearInterval(interval);

    return true;
  }

  c++;
}





// use this line if you want it to visually walk through the path
interval = setInterval(walk);

// use this one if you want to just draw the whole thing at once
// while (true) {
//   let result = walk();

//   if (result) {
//     break;
//   }
// }
